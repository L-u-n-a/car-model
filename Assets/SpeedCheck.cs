﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedCheck : MonoBehaviour {

	private float startDistance;
	public float distanceTraveled;

	private float clock;
	private float seconds;

	private bool timing;

	void Awake() {
        Application.targetFrameRate = 60;
    }

	// Use this for initialization
	void Start () {
		startDistance = this.transform.position.z;
		distanceTraveled = 0;

		clock = 0f;
		seconds = 0f;

		timing = true;
	}
	
	// Update is called once per frame
	void Update () {
		distanceTraveled = this.transform.position.z - startDistance;

		updateTime();
		endJourney();
	}

	void updateTime() {
		if(timing) {
			clock += Time.deltaTime;
 			seconds = clock % 60;
		}
	}

	void endJourney() {
		if(seconds >= 6) {
			timing = false;
			//Debug.Log("Seconds: " + seconds + " Distance Traveled: " + distanceTraveled);
			//Debug.Log("Velocity: " + rb.velocity.z);
		}
	}
}
