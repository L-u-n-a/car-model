﻿     

    using UnityEngine;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine.UI;
    
    public class SimpleCarController : MonoBehaviour {
       public List<AxleInfo> axleInfos; 
        public float maxMotorTorque;
        public float maxSteeringAngle;

        /* This is the speed we are driving at in km/h. This will be displayed on the screen.  */
        public Text kmh;

        /* Used to ensure the car does not drive faster then it's maximum speed limit. */
        public float maxSpeed;

        /* Used to select the RigidBody of the car (see Start() method). */
        public Rigidbody rb;

        /* A number that determines how effective the brakes are. */
        private float brakes;

        /* We need this set of vectors and the boolean to check if our car is going forwards or backwards. */
        private Vector3 movement;
        private Vector3 LastPOS;
        private Vector3 NextPOS;
        private Vector3 fwd;
        private bool forward;

        void Start() {
            rb = GetComponent<Rigidbody>();

            brakes = 9000;
        }
     
        // finds the corresponding visual wheel
        // correctly applies the transform
        public void ApplyLocalPositionToVisuals(WheelCollider collider) {

            if (collider.transform.childCount == 0) {
                return;
            }
     
            Transform visualWheel = collider.transform.GetChild(0);
     
            Vector3 position;
            Quaternion rotation;
            collider.GetWorldPose(out position, out rotation);
     
            visualWheel.transform.position = position;
            visualWheel.transform.rotation = rotation;
        }
     
        public void FixedUpdate() {

            float motor = maxMotorTorque * Input.GetAxis("Vertical");
            float steering = maxSteeringAngle * Input.GetAxis("Horizontal");
     
            foreach (AxleInfo axleInfo in axleInfos) {
                if (axleInfo.steering) {
                    axleInfo.leftWheel.steerAngle = steering;
                    axleInfo.rightWheel.steerAngle = steering;
                }

                if(rb.velocity.magnitude < maxSpeed && axleInfo.motor) {
                    axleInfo.leftWheel.motorTorque = motor;
                    axleInfo.rightWheel.motorTorque = motor;
                }
                else {
                    axleInfo.leftWheel.motorTorque = maxSpeed;
                    axleInfo.rightWheel.motorTorque = maxSpeed;
                }

                ApplyLocalPositionToVisuals(axleInfo.leftWheel);
                ApplyLocalPositionToVisuals(axleInfo.rightWheel);
                ApplyBrakes(axleInfo);
            }

            UpdateSpeed();
        }

        void ApplyBrakes(AxleInfo axleInfo) {
            CheckDirection();

            if(Input.GetKey(KeyCode.S) && CheckDirection() && rb.velocity.magnitude > 1f) {
                axleInfo.leftWheel.brakeTorque = brakes;
                axleInfo.rightWheel.brakeTorque = brakes;
            }
            else if(Input.GetKey(KeyCode.S) && !CheckDirection()) {
                axleInfo.leftWheel.brakeTorque = 0;
                axleInfo.rightWheel.brakeTorque = 0;
            }

            if(Input.GetKey(KeyCode.W) && !CheckDirection() && rb.velocity.magnitude > 1f) {
                axleInfo.leftWheel.brakeTorque = brakes;
                axleInfo.rightWheel.brakeTorque = brakes;
            }
            else if(Input.GetKey(KeyCode.W) && CheckDirection()) {
                axleInfo.leftWheel.brakeTorque = 0;
                axleInfo.rightWheel.brakeTorque = 0;
            }
        }

        bool CheckDirection() {
            NextPOS = transform.position;
            movement = (NextPOS - LastPOS);

            if(Vector3.Dot(fwd, movement) < 0){
                //Debug.Log("Moving backwards");
                forward = false;
            }
            else if(Vector3.Dot(fwd, movement) > 0){
                //Debug.Log("Moving forwards");
                forward = true;
            }

            LateUpdate();

            if(forward) {
                return true;
            }
            else {
                return false;
            }
        }

        void LateUpdate() {
            LastPOS = transform.position;
            fwd = transform.forward;
        }

        void UpdateSpeed() {
            float speed = rb.velocity.magnitude * 3.6f;
            kmh.text = speed.ToString("f0") + " km/h";
        } 
    }
    
    [System.Serializable]
    public class AxleInfo {
        public WheelCollider leftWheel;
        public WheelCollider rightWheel;
        public bool motor; // is this wheel attached to motor?
        public bool steering; // does this wheel apply steer angle?
    }